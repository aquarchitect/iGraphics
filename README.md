InteractiveGraphics
===================

InteractiveGraphics is an application that transmits interactive data between mobile devices and Processing sketches via a web socket.

Future plans:
- [ ] Present Processing parameter setting on mobile screen.
- [ ] Mac desktop application with shader and peer connectivity