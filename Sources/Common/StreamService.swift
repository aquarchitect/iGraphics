//
//  StreamService.swift
//  iRemote
//
//  Created by Hai Nguyen on 1/2/16.
//  Copyright © 2016 Hai Nguyen. All rights reserved.
//

final class StreamService: MyKit.StreamService {

    private static let HostKey = (ip: "IP", port: "Port")

    enum Message {

        case Size(CGSize)
        case Point(CGPoint)
    }

    func send(message: Message) {
        var data: NSData?
        switch message {

        case .Size(let size): data = size.data
        case .Point(let point): data = point.data
        }

        data?.then(write)
    }
}

private extension CGFloat {

    var string: String { return String(format: "%.3f", self) }
}

private extension CGSize {

    var data: NSData? {
        let json = "{\"size\": {\"w\": \(self.width.string), \"h\": \(self.height.string)}}"
        return json.dataUsingEncoding(NSUTF8StringEncoding)
    }
}

private extension CGPoint {

    var data: NSData? {

        let json = "{\"coord\": {\"x\": \(self.x.string), \"y\": \(self.y.string), \"z\": \(0)}}"
        return json.dataUsingEncoding(NSUTF8StringEncoding)
    }
}

