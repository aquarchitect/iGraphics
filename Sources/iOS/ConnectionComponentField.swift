//
//  ConnectionComponentField.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/26/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

final class ConnectionComponentField: UITextField {

    private let numberOfDigits: Int

    private var indicatorEnabled = false {
        didSet { self.setNeedsDisplay() }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(numberOfDigits count: Int) {
        self.numberOfDigits = count

        let string = String([Character](count: count, repeatedValue: "\u{0030}"))
        let placeholder = NSMutableAttributedString(string: string)
                        .then { $0.addColor(UIColor(white: 0.9, alpha: 1)) }

        super.init(frame: .zero)
        super.tintColor = .clearColor()
        super.attributedPlaceholder = placeholder
        super.backgroundColor = .clearColor()
        super.font = .systemFontOfSize(20, weight: UIFontWeightLight)
        super.textAlignment = .Right
        super.keyboardType = .NumberPad
        super.keyboardAppearance = .Dark
        super.delegate = self
    }

    override func intrinsicContentSize() -> CGSize {
        return UILabel.dummyInstance.then {
            $0.font = self.font
            $0.text = (self.text ?? "").isEmpty ? self.placeholder : self.text
            $0.sizeToFit()
            return $0.bounds.size
        }
    }

    override func becomeFirstResponder() -> Bool {
        let result = super.becomeFirstResponder()

        if result {
            self.text = nil
            indicatorEnabled = true
        }
        return result
    }

    override func resignFirstResponder() -> Bool {
        let result = super.resignFirstResponder()
        if result { indicatorEnabled = false }
        return result
    }

    override func drawRect(rect: CGRect) {
        guard indicatorEnabled else { return }
        let points = [rect.minX, rect.maxX].map { CGPointMake($0, rect.maxY) }

        UIColor.darkGrayColor().setStroke()
        UIBezierPath(points: points).stroke()
    }
}

extension ConnectionComponentField: UITextFieldDelegate {

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let count = textField.text?.characters.count ?? 0
        if range.length + range.location > count { return false }

        let newLength = count + string.characters.count - range.length
        return newLength <= numberOfDigits
    }
}