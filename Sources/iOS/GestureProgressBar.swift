//
//  GestureProgressBar.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/28/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

final class GestureProgessBar: UIView {

    static let Height: CGFloat = 4
    enum Connection { case Error, InProgress, Connected }

    var connection: Connection = .Error {
        willSet {
            statusLine.strokeColor = newValue.color.CGColor

            if case .InProgress = newValue {
                CATransaction.begin()
                CATransaction.setDisableActions(true)
                statusLine.strokeEnd = 0.2
                CATransaction.commit()

                animateProgress()
            } else {
                statusLine.removeAllAnimations()
                statusLine.strokeStart = 0
                statusLine.strokeEnd = 1
            }
        }
    }

    private let statusLine = CAShapeLayer().then {
        $0.fillColor = nil
        $0.lineWidth = GestureProgessBar.Height
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        super.init(frame: .zero)
        super.translatesAutoresizingMaskIntoConstraints = false
        super.layer.addSublayer(statusLine)

        NSLayoutConstraint.constraints(withFormat: "V:[self(height)]", views: ["self": self], metrics: ["height": self.dynamicType.Height]).activate()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let points = [self.bounds.minX, self.bounds.maxX].map { CGPointMake($0, self.bounds.midY) }
        statusLine.path = UIBezierPath(points: points).CGPath
    }

    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if (anim as? CABasicAnimation)?.keyPath == "strokeStart" && flag { animateProgress() }
    }

    private func animateProgress() {
        ["strokeStart": 0.8, "strokeEnd": 1].forEach { (key, value) in
            CABasicAnimation().then {
                $0.keyPath = key
                $0.duration = 1
                $0.autoreverses = true
                $0.delegate = self
                $0.toValue = NSNumber(double: value)
            }.then {
                statusLine.addAnimation($0, forKey: nil)
            }
        }
    }
}

private extension GestureProgessBar.Connection {

    var color: UIColor {
        return self == .Connected ? .greenColor() : .redColor()
    }
}