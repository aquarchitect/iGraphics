//
//  ConnectionClearButton.swift
//  iRemote
//
//  Created by Hai Nguyen on 1/1/16.
//  Copyright © 2016 Hai Nguyen. All rights reserved.
//

final class ConnectionClearButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        let title = SymbolIcon("\u{F406}")
            .attributedString(ofSize: 18)
            .then { $0.addColor(.lightGrayColor()) }

        super.init(frame: .zero)
        super.backgroundColor = .clearColor()
        super.setAttributedTitle(title, forState: .Normal)
        super.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.constraints(withFormat: "H:[self(35)]", views: ["self": self])
    }
}