//
//  GestureMainView.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/26/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

final class GestureMainView: UIView {

    private var touchLocation: CGPoint? {
        didSet {
            func redrawForPoint(point: CGPoint?) {
                let rect = rectForTouch(point)
                self.setNeedsDisplayInRect(rect)
            }

            redrawForPoint(oldValue)
            redrawForPoint(touchLocation)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        super.init(frame: .zero)
        super.backgroundColor = .blackColor()
    }

    override func addGestureRecognizer(gestureRecognizer: UIGestureRecognizer) {
        gestureRecognizer.addTarget(self, action: #selector(handleGesture(_:)))
        super.addGestureRecognizer(gestureRecognizer)
    }

    override func drawRect(rect: CGRect) {
        let circleWidth: CGFloat = 1

        var touchRect = rectForTouch(touchLocation)
        touchRect = CGRectInset(touchRect, circleWidth / 2, circleWidth / 2)

        UIColor(white: 0.3, alpha: 1).setStroke()
        UIBezierPath(ovalInRect: touchRect).then {
            $0.lineWidth = circleWidth
            $0.stroke()
        }
    }

    private func rectForTouch(location: CGPoint?) -> CGRect {
        guard let location = location else { return .zero }
        let rect = CGRect(origin: location, size: .zero)
        return CGRectInset(rect, -30, -30)
    }

    func handleGesture(sender: UIGestureRecognizer) {
        let location = sender.locationInView(self)
        switch sender.state {

        case .Began, .Changed: touchLocation = location
        default: touchLocation = nil
        }
    }
}
