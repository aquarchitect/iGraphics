//
//  GestureViewController.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/26/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

final class GestureViewController: UIViewController {

    // MARK: Property

    let streamService: StreamService
    let progressBar = GestureProgessBar()
    let connectionButton = GestureConnectionButton()

    // MARK: View Lifecycle

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(streamService service: StreamService) {
        self.streamService = service
        super.init(nibName: nil, bundle: nil)
    }

    // MARK: View Lifecycle

    override func loadView() {
        view = GestureMainView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        connectionButton.addTarget(self, action: #selector(handleConnectionButton(_:)), forControlEvents: .TouchUpInside)

        let subviews = ["connectionButton": connectionButton, "progressBar": progressBar]
        let metrics = ["bottomMargin": GestureProgessBar.Height]
        subviews.values.forEach(view.addSubview)

        ["H:[connectionButton]|", "H:|[progressBar]|", "V:[connectionButton]-bottomMargin-|", "V:[progressBar]|"].reduce([]) { $0 + NSLayoutConstraint.constraints(withFormat: $1, views: subviews, metrics: metrics) }.activate()

        NSNotificationCenter.defaultCenter().then {
            $0.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
            $0.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
            $0.addObserver(self, selector: #selector(handleStreamClient(_:)), name: StreamService.EventNotification, object: nil)
        }

        UIPanGestureRecognizer().then {
            $0.addTarget(self, action: #selector(handlePan(_:)))
            $0.minimumNumberOfTouches = 1
            view.addGestureRecognizer($0)
        }
    }

    // MARK: System Method

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    func animateAlongsideTransition(presenting: Bool) {
        self.transitionCoordinator()?.animateAlongsideTransition({ [unowned self] _ in
            if presenting {
                self.connectionButton.alpha = 0
                self.progressBar.transform = CGAffineTransformMakeTranslation(0, -ConnectionMainView.Height)
            } else {
                self.connectionButton.alpha = 1
                self.progressBar.transform = CGAffineTransformIdentity
            }
        }, completion: nil)
    }

    override func dismissViewControllerAnimated(flag: Bool, completion: (() -> Void)?) {
        super.dismissViewControllerAnimated(flag, completion: completion)
        if self.presentedViewController is ConnectionViewController {
            animateAlongsideTransition(false)
        }
    }

    // MARK: Action Listenser

    func handleConnectionButton(sender: UIButton) {
        ConnectionViewController(streamService: streamService).then {
            self.presentViewController($0, animated: true, completion: nil)
            self.animateAlongsideTransition(true)
        }
    }

    func handlePan(sender: UIPanGestureRecognizer) {
        switch sender.state {

        case .Changed:
            let location = sender.normalizedLocationIn(view)
            let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
            dispatch_async(queue) { self.streamService.send(.Point(location)) }

        case let state:
            let options: UIViewAnimationOptions = state == .Began ? [.CurveEaseIn, .AllowUserInteraction, .BeginFromCurrentState] : [.CurveEaseOut, .BeginFromCurrentState]
            let transform = state == .Began ? CGAffineTransformMakeTranslation(0, self.view.bounds.height) : CGAffineTransformIdentity

            UIView.animateWithDuration(0.25, delay: 0, options: options, animations: {
                self.connectionButton.transform = transform
            }, completion: nil)
        }
    }

    func handleStreamClient(notificaiton: NSNotification) {
        switch (notificaiton.object as? Box<NSStreamEvent>)?.value {

        case [.OpenCompleted]?:
            progressBar.connection = .Connected

            let size = UIScreen.mainScreen().bounds.size
            streamService.send(.Size(size))

            if self.presentedViewController != nil {
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        case let options? where !options.isDisjointWith([.ErrorOccurred, .EndEncountered]):
            progressBar.connection = .Error
        default: break
        }
    }

    // MARK: Keyboard Notification

    func keyboardWillShow(notification: NSNotification) {
        guard let rect = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey]?.CGRectValue else { return }
        progressBar.transform = CGAffineTransformMakeTranslation(0, -rect.height - ConnectionMainView.Height)
    }

    func keyboardWillHide(notification: NSNotification) {
        progressBar.transform = self.presentedViewController == nil ? CGAffineTransformIdentity : CGAffineTransformMakeTranslation(0, -ConnectionMainView.Height)
    }
}

private extension UIGestureRecognizer {

    func normalizedLocationIn(view: UIView) -> CGPoint {
        return view.then { self.locationInView($0).nomalizedPointIn($0.bounds.size) }
    }
}

private extension CGPoint {

    func nomalizedPointIn(size: CGSize) -> CGPoint {
        let x = size.width == 0 ? 0 : self.x / size.width
        let y = size.height == 0 ? 0 : self.y / size.height
        return CGPointMake(x, y)
    }
}