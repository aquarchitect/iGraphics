//
//  ConnectionIPAddressField.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/26/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

final class ConnectionIPAddressField: UIStackView {

    let componentFields = [3, 3, 3, 3, 5].map {
        ConnectionComponentField(numberOfDigits: $0)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        super.init(frame: .zero)
        super.axis = .Horizontal
        super.alignment = .FirstBaseline
        super.distribution = .Fill
        super.translatesAutoresizingMaskIntoConstraints = false

        componentFields.enumerate().forEach {
            var separator = "."

            switch $0 {

            case 0: self.addArrangedSubview($1)
            case 4: $1.textAlignment = .Left; separator = ":"; fallthrough
            default: [self.separatorLabel(separator), $1].forEach(self.addArrangedSubview)
            }
        }
    }

    override func becomeFirstResponder() -> Bool {
        return componentFields.first?.becomeFirstResponder() == true
    }

    private func separatorLabel(string: String) -> UILabel {
        return UILabel().then {
            $0.font = .systemFontOfSize(20)
            $0.text = string
            $0.textColor = .blackColor()
            $0.textAlignment = .Center
            $0.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint(view: $0, attribute: .Width, relatedBy: .Equal, toView: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 10).active = true
        }
    }
}