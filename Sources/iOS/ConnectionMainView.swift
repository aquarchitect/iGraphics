//
//  ConnectionMainView.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/27/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

final class ConnectionMainView: UIView {

    static let Height: CGFloat = 50
    private let componentFields: [ConnectionComponentField]

    var host: StreamService.Host? {
        get {
            guard let ip = self.ip, port = self.port else { return nil }
            return try? StreamService.Host(ip: ip, port: port)
        }
        set {
            self.ip = newValue?.ip
            self.port = newValue?.port
        }
    }

    private var ip: String? {
        get {
            guard case let fields = componentFields.dropLast()
                where (fields.filter { ($0.text ?? "").isEmpty }).count == 0
                else { return nil }
            return fields.flatMap { $0.text }.joinWithSeparator(".")
        }
        set {
            var components = newValue?.componentsSeparatedByString(".")
            if !(components?.count == 4) { components = Array(count: 4, repeatedValue: "") }
            components?.enumerate().forEach { self.componentFields[$0].text = $1 }
        }
    }

    private var port: UInt32? {
        get { return UInt32(componentFields.last?.text ?? "") }
        set {
            if let port = newValue {
                componentFields.last?.text = "\(port)"
            } else { componentFields.last?.text = "" }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        let clearButton = ConnectionClearButton()
        let ipAddressField = ConnectionIPAddressField()

        self.componentFields = ipAddressField.componentFields

        super.init(frame: .zero)
        super.backgroundColor = .whiteColor()
        super.translatesAutoresizingMaskIntoConstraints = false

        clearButton.addTarget(self, action: #selector(handleButton(_:)), forControlEvents: .TouchUpInside)
        [clearButton, ipAddressField].forEach(self.addSubview)

        let views = ["self": self, "clearButton": clearButton]
        let metrics = ["height": self.dynamicType.Height]

        ["H:[clearButton]|", "V:|[clearButton]|", "V:[self(height)]"].reduce([]) { $0 + NSLayoutConstraint.constraints(withFormat: $1, views: views, metrics: metrics) }.activate()
        [.CenterX, .CenterY].map { NSLayoutConstraint(view: ipAddressField, attribute: $0, relatedBy: .Equal, toView: self, attribute: $0, multiplier: 1, constant: 0) }.activate()

        NSNotificationCenter.defaultCenter().then {
            $0.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
            $0.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
            $0.addObserver(self, selector: #selector(textFieldDidChange(_:)), name: UITextFieldTextDidChangeNotification, object: nil)
        }
    }

    override func becomeFirstResponder() -> Bool {
        return componentFields.first?.becomeFirstResponder() == true
    }

    func keyboardWillShow(notification: NSNotification) {
        guard let rect = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey]?.CGRectValue else { return }
        self.transform = CGAffineTransformMakeTranslation(0, -rect.height)
    }

    func keyboardWillHide(notification: NSNotification) {
        self.transform = CGAffineTransformIdentity
    }

    func textFieldDidChange(notification: NSNotification) {
        guard let textField = notification.object as? UITextField else { return }
        textField.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
    }

    func handleButton(sender: UIButton) {
        ip = nil; port = nil
    }
}