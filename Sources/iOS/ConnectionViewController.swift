//
//  ConnectionViewController.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/26/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

final class ConnectionViewController: UIViewController {

    let transition: TransitionBasicAnimator = {
        let rect = UIScreen.mainScreen().bounds
        let height = ConnectionMainView.Height

        return TransitionBasicAnimator(customRect: rect).then {
            $0.animating.transform = CGAffineTransformMakeTranslation(0, height)
            $0.dimming = (0, true)
        }
    }()

    private let streamService: StreamService
    private var timeoutID: Schedule.ID?
    private let mainView = ConnectionMainView()

    private var progressBar: GestureProgessBar? {
        return (self.presentingViewController as? GestureViewController)?.progressBar
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(streamService service: StreamService) {
        self.streamService = service

        super.init(nibName: nil, bundle: nil)
        super.modalPresentationStyle = .Custom
        super.transitioningDelegate = transition
    }

    override func loadView() {
        class View: UIView {

            private override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
                let view = super.hitTest(point, withEvent: event)
                return view === self ? nil : view
            }
        }

        view = View().then {
            $0.backgroundColor = .clearColor()
            $0.addSubview(mainView)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        streamService.then {
            mainView.host = $0.host
            $0.reconnect()
        }

        ["H:|[mainView]|", "V:[mainView]|"].reduce([]) { $0 + NSLayoutConstraint.constraints(withFormat: $1, views: ["mainView": mainView]) }.activate()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(textFieldDidChange(_:)), name: UITextFieldTextDidChangeNotification, object: nil)
    }

    override func viewDidAppear(animated: Bool) {
        guard mainView.host == nil else { return }
        delay(0.2) { self.mainView.becomeFirstResponder() }
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    func textFieldDidChange(notification: NSNotification) {
        Schedule.cancel(timeoutID ?? 0)

        if let host = mainView.host {
            Schedule.once(0) { [unowned self] in
                self.streamService.connectTo(host)
                self.progressBar?.connection = .InProgress
            }
        } else {
            progressBar?.connection = .Error
            streamService.disconnect()
        }
    }
}