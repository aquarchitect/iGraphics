//
//  AppDelegate.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/26/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

@UIApplicationMain
final class AppDelegate: UIResponder {

    var window: UIWindow?

    private let streamService = StreamService()
}

extension AppDelegate: UIApplicationDelegate {

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        let controller = GestureViewController(streamService: streamService)

        dispatch_async(Queue.Main) {
            if let host = NSUserDefaults.standardUserDefaults().lastHost {
                self.streamService.connectTo(host)
                controller.progressBar.connection = .InProgress
            } else {
                controller.progressBar.connection = .Error
            }
        }

        window = UIWindow(frame: UIScreen.mainScreen().bounds).then {
            $0.rootViewController = controller
            $0.backgroundColor = .blackColor()
            $0.makeKeyAndVisible()
        }

        return true
    }

    func applicationDidEnterBackground(application: UIApplication) {
        streamService.disconnect()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        window?.rootViewController?.then {
            ($0 as? GestureViewController)?.progressBar
        }?.then {
            streamService.reconnect()
            $0.connection = .InProgress
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        streamService.disconnect()

        if let host = streamService.host {
            NSUserDefaults.standardUserDefaults().cache(host)
        }
    }
}

private extension NSUserDefaults {

    private static let HostKey = (ip: "IP", port: "Port")

    var lastHost: StreamService.Host? {
        let key = self.dynamicType.HostKey

        guard let ip = self.valueForKey(key.ip) as? String,
            port = UInt32(self.valueForKey(key.port) as? String ?? "")
            else { return nil }
        return try? StreamService.Host(ip: ip, port: port)
    }

    func cache(host: StreamService.Host) {
        let key = self.dynamicType.HostKey
        [key.ip: host.ip, key.port: "\(host.port)"].forEach {
            self.setValue($1, forKey: $0)
        }
    }

    func removeLastHost() {
        let key = self.dynamicType.HostKey
        [key.ip, key.port].forEach(self.removeObjectForKey)
    }
}