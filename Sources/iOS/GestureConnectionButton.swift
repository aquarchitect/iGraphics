//
//  GestureConnectionButton.swift
//  GestureController
//
//  Created by Hai Nguyen on 12/26/15.
//  Copyright © 2015 Hai Nguyen. All rights reserved.
//

final class GestureConnectionButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        let title = SymbolIcon("\u{F13E}")
            .attributedString(ofSize: 35)
            .then {
                $0.addColor(.darkGrayColor())
                $0.addAlignment(.Center)
            }

        super.init(frame: .zero)
        super.backgroundColor = .clearColor()
        super.setAttributedTitle(title, forState: .Normal)
        super.translatesAutoresizingMaskIntoConstraints = false

        [NSLayoutConstraint(view: self, attribute: .Height, relatedBy: .Equal, toView: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 44),
         NSLayoutConstraint(view: self, attribute: .Width, relatedBy: .Equal, toView: self, attribute: .Height, multiplier: 1, constant: 0)].activate()
    }
}